"use strict";

/**
 * Main
 */
jQuery( function( $ ) {
    $( 'body' )
        .on( 'click', '[data-href]', function( e ) {
            if( !e.isDefaultPrevented() ) {
                if( $( this ).is( '[data-target]' ) ) {
                    var newWin = window.open( $( this ).attr( 'data-href' ), $( this ).attr( 'data-target' ) );
                    if( newWin ) {
                        newWin.focus();
                    }
                } else {
                    window.location.href = $( this ).attr( 'data-href' );
                }
            }
        } );

} );
