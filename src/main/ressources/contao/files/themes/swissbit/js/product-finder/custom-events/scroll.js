"use strict";

jQuery( function( $ ) {

    var $document = $( document ),
        lastScrollPosition = $document.scrollTop(),
        scrolling = false,
        intervalId = 0,
        delta = 0,
        direction = null;

    function scrollStartHandler()
    {
        if( !scrolling ) {
            intervalId = setInterval( function() {
                var newScrollPosition = $document.scrollTop();

                delta = (newScrollPosition - lastScrollPosition);

                if( 0 === delta ) {
                    clearInterval( intervalId );
                    setTimeout( function() {
                        intervalId = 0;
                        scrolling = false;
                        $document.trigger( 'scroll-stop', [direction, newScrollPosition] );

                        $document.one( 'scroll', scrollStartHandler );
                    } );
                }

                lastScrollPosition = newScrollPosition;
            }, 100 );
        }

        scrolling = true;
        $document.trigger( 'scroll-start', [direction, lastScrollPosition] );
    };

    $document.one( 'scroll', scrollStartHandler );
} );
