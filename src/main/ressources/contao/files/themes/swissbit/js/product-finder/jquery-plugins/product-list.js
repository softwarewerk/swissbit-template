"use strict";

/**
 * Plugin Closure
 */
(function( $ ) {
    /**
     * @param {string} method
     */
    jQuery.fn.swissbitProductList = function( method ) {
        var args = arguments;
        return this.each( function() {
            var plugin = $( this ).data( 'plugin' )

            if( !plugin ) {
                $( this ).data( 'plugin', plugin = new Plugin( this, args.length ? args[0] : null ) );
            }

            if( plugin[method] ) {
                return plugin[method].apply( plugin, Array.prototype.slice.call( args, 1 ) );
            } else if( method ) {
                $.error( 'Method ' + method + ' does not exist on jQuery.swissbitProductList' );
            }
        } );
    };

    /**
     * @param {Object}
     */
    jQuery.fn.swissbitProductList.defaults = {};

    /**
     * @param {HTMLElement} elem
     * @param {Array} options
     */
    function Plugin( elem, options )
    {
        options = $.extend( {
                'limit': parseInt( $( elem ).attr( 'data-page-size' ) || 0 ),
                'productListApi': $( elem ).attr( 'data-product-list-api' ),
                'productListApiReset': $( elem ).attr( 'data-product-list-api-reset' ),
                'productCompareApi': $( elem ).attr( 'data-product-compare-api' ),
                'productAvailabilityApi': $( elem ).attr( 'data-availability-api' )
            },
            $.fn.swissbitProductList.defaults,
            $.isPlainObject( options ) ? method : {}
        );

        var self = this;

        this.elem = elem;

        this.productListApi = options.productListApi;
        this.productListApiReset = options.productListApiReset;

        this.limit = options.limit;
        this.offset = 0;
        this.totalItemsRendered = 0;

        this.isLoadingFlag = false;
        this.isCompleteFlag = false;
        this.isEmptyFlag = false;

        this.resetNotificationActive = false;
        this.itemToggleNotificationActive = false;
        this.loadingNotificationActive = false;
        this.loadCompleteNotificationActive = false;
        this.completeNotificationActive = false;

        this.selectedItemIds = [];

        $( 'body' ).on( 'change.filter', 'form', $.proxy( function( e ) {
            if( !e.isDefaultPrevented() && 'filter' == e.namespace ) {
                this.reset();
            }
        }, this ) );

        // Product list
        $( elem )

        // Add class to clicked table rows to allow css styles
            .on( 'click', 'tr[data-item-id]:not(.clicked)', function() {
                $( this ).addClass( 'clicked' );
                setTimeout( $.proxy( function() {
                    $( this ).removeClass( 'clicked' );
                }, this ), 200 );
            } )

            .on( 'click', 'tr[data-item-id]', function( e ) {

                if( $( e.target ).is( 'td:first-child, td:first-child *' ) ) {
                    e.preventDefault();
                    $.getJSON( options.productCompareApi, { 'id': $( this ).attr( 'data-item-id' ) },
                        $.proxy( function( response ) {
                            if( response.error ) {
                                $( '#Modal' ).swissbitModal( 'alert', response.error_message );
                                return;
                            } else {
                                $( this ).toggleClass( 'selected', response.selected );
                            }

                            self.selectedItemIds = response.selected_product_ids;
                            self.notifyItemToggle();
                        }, this ) );
                }
            } )
            .on( 'loading.product-list.swissbit', function() {
                $( this ).addClass( 'product-list-loading' );
            } )
            .on( 'load-complete.product-list.swissbit', function() {
                $( this ).removeClass( 'product-list-loading' );

                $( this )
                    .toggleClass( 'product-list-complete', self.isComplete() )
                    .toggleClass( 'product-list-empty', self.isEmpty() );

                setTimeout( $.proxy( function() {
                    $( '.product-list-table, .product-list-grid', this ).each( function() {
                        var offset = 0;
                        $( '.fade:not(.in)', this ).each( function() {
                            setTimeout( $.proxy( function() {
                                $( this ).addClass( 'in' );
                            }, this ), offset += 20 );
                        } );
                    } );
                }, this ) );
            } )
            .on( 'reset.product-list.swissbit', function() {
                $( this )
                    .removeClass( 'product-list-complete' )
                    .removeClass( 'product-list-empty' );
            } )

            // Update availability
            .on( 'load-complete.product-list.swissbit', function() {
                // poor mans optimization
                setTimeout( $.proxy( function() {
                    var $items = $( '[data-item-part-number].item-availability:not(.loaded, .loading)', elem )
                            .html( '<i class="fas fa-circle-notch fa-spin"></i>' )
                            .addClass( 'loading' ),
                        productPartNumbers = [];

                    for( var i = 0, item; item = $items.get( i ); i++ ) {
                        productPartNumbers.push( $( item ).attr( 'data-item-part-number' ) );
                    }

                    $.post( options.productAvailabilityApi, { 'mpn': productPartNumbers }, function( response ) {

                        $.each( response, function( mpn, rating ) {

                            // poor mans optimization
                            setTimeout( $.proxy( function() {
                                rating = parseFloat( rating || 0 );
                                $items.filter( '[data-item-part-number="' + mpn + '"]' )
                                    .removeClass( 'loading' )
                                    .attr( 'data-rating', rating )
                                    .html( function() {
                                        var html = '',
                                            rating = parseInt( $( this ).attr( 'data-rating' ) );
                                        for( var i = 0; i < 5; i++ ) {
                                            if( 1 <= rating ) {
                                                html += '<i class="availability-star availability-star-full fade"></i>';
                                            } else if( 0 < rating ) {
                                                html += '<i class="availability-star availability-star-half fade"></i>';
                                            } else {
                                                html += '<i class="availability-star availability-star fade"></i>';
                                            }

                                            rating -= 1;
                                        }
                                        return html;
                                    } )
                                    .addClass( function() {
                                        $( '.availability-star.fade' ).addClass( 'in' );
                                        return 'loaded';
                                    } );
                                // .addClass( function() {
                                //     var timeout = 0;
                                //     $( '.availability-star', this ).each( function() {
                                //         setTimeout( $.proxy( function() {
                                //             $( this ).addClass( 'in' );
                                //         }, this ), timeout += 50 );
                                //     } );
                                //     return 'loaded';
                                // } );
                            }, this ) );

                        } );

                        // Test
                        $items.filter( '[data-item-part-number].loading' )
                            .removeClass( 'loading' )
                            .addClass( 'loaded' )
                            .html( 'n/a' );
                    } );
                }, this ) );
            } );

        /**
         * Initialize infinite loading
         */
        $( '.loader.loader-autoload', elem ).addClass( function() {
            if( $( this ).offset().top <= $( window ).height() ) {
                self.load();
            }

            $( document ).on( 'scroll-stop', $.proxy( function() {
                if( !self.isLoadingFlag ) {
                    var scrollTop = $( window ).scrollTop(),
                        windowHeight = $( window ).height(),
                        elemBottom = $( this ).offset().top + $( this ).height();

                    if( elemBottom <= (scrollTop + windowHeight) ) {
                        self.load();
                    }
                }
            }, this ) );

            return 'loader-active';
        } );
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.notifyItemToggle = function() {
        if( this.itemToggleNotificationActive ) {
            return this;
        }

        this.itemToggleNotificationActive = true;

        // Wait for the current execution chain to finish first!
        setTimeout( $.proxy( function() {
            $( this.elem ).trigger( 'item-toggle.product-list.swissbit', [this, this.getSelectedItemIds()] );
            this.itemToggleNotificationActive = false;
        }, this ) );

        return this;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.notifyReset = function() {
        if( this.resetNotificationActive ) {
            return this;
        }

        this.resetNotificationActive = true;

        // Wait for the current execution chain to finish first!
        setTimeout( $.proxy( function() {
            $( this.elem ).trigger( 'reset.product-list.swissbit', [this] );
            this.resetNotificationActive = false;
        }, this ) );

        return this;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.notifyComplete = function() {
        if( this.completeNotificationActive ) {
            return this;
        }

        this.completeNotificationActive = true;

        // Wait for the current execution chain to finish first!
        setTimeout( $.proxy( function() {
            $( this.elem ).trigger( 'complete.product-list.swissbit', [this] );
            this.completeNotificationActive = false;
        }, this ) );

        return this;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.notifyLoading = function() {
        if( this.loadingNotificationActive ) {
            return this;
        }

        this.loadingNotificationActive = true;

        // Wait for the current execution chain to finish first!
        setTimeout( $.proxy( function() {
            $( this.elem ).trigger( 'loading.product-list.swissbit', [this] );
            this.loadingNotificationActive = false;
        }, this ) );

        return this;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.notifyLoadComplete = function() {
        if( this.loadCompleteNotificationActive ) {
            return this;
        }

        this.loadCompleteNotificationActive = true;

        // Wait for the current execution chain to finish first!
        setTimeout( $.proxy( function() {
            $( this.elem ).trigger( 'load-complete.product-list.swissbit', [this] );
            this.loadCompleteNotificationActive = false;
        }, this ) );

        return this;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.setIsComplete = function( flag ) {
        this.isCompleteFlag = (true === flag);
    };

    /**
     * @return {boolean}
     */
    Plugin.prototype.isComplete = function() {
        return true === this.isCompleteFlag;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.setIsEmpty = function( flag ) {
        this.isEmptyFlag = (true === flag);
    };

    /**
     * @return {boolean}
     */
    Plugin.prototype.isEmpty = function() {
        return true === this.isEmptyFlag;
    };

    /**
     * @return {Array<number>}
     */
    Plugin.prototype.getSelectedItemIds = function() {
        return this.selectedItemIds;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.load = function() {
        if( this.isComplete() || this.isLoadingFlag ) {
            return this;
        }

        this.isLoadingFlag = true;
        this.notifyLoading();

        // TODO: enhance error handling!
        $.getJSON( this.productListApi, {
            'o': this.offset,
            'n': this.limit
        }, $.proxy( function( response ) {
            if( 0 < response.items ) {
                $( 'table.product-list-table:not(.clone)', this.elem ).append( response.html['table_format'] );
                $( '.product-list-grid', this.elem ).append( response.html['grid_format'] );
                delete response.html;
            }

            if( 0 == this.offset && 0 == response.items ) {
                this.setIsEmpty( true );
                this.setIsComplete( true );
            } else if( response.items < this.limit ) {
                this.setIsComplete( true );
            }

            this.totalItemsRendered += response.items;
            this.offset += this.limit;
            $( this.elem ).attr( 'data-offset', this.offset );
            $( this.elem ).attr( 'data-total-rendered-items', this.totalItemsRendered );

            this.isLoadingFlag = false;
            this.notifyLoadComplete();

            if( this.isComplete() ) {
                this.notifyComplete();
            }
        }, this ) );

        return this;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.reset = function() {
        $.getJSON( this.productListApiReset, $.proxy( function() {
            $( '.product-group', this.elem ).remove();
            $( '[data-item-id]', this.elem ).remove();

            this.offset = 0;
            this.totalItemsRendered = 0;

            this.isCompleteFlag = false;
            this.isEmptyFlag = false;

            this.notifyReset();

            this.load();
        }, this ) );

        return this;
    };
})( jQuery );
