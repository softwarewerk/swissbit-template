"use strict";

/**
 * Plugin Closure
 */
(function( $ ) {
    /**
     * @param {string} method
     */
    jQuery.fn.swissbitFilter = function( method ) {
        var args = arguments;
        return this.each( function() {
            var plugin = $( this ).data( 'plugin' );

            if( !plugin ) {
                $( this ).data( 'plugin', plugin = new Plugin( this, args.length ? args[0] : null ) );
            }

            if( plugin[method] ) {
                return plugin[method].apply( plugin, Array.prototype.slice.call( args, 1 ) );
            } else if( method ) {
                $.error( 'Method ' + method + ' does not exist on jQuery.swissbitFilter' );
            }
        } );
    };

    /**
     * @type {Object}
     */
    jQuery.fn.swissbitFilter.defaults = {};

    /**
     * @param {HTMLElement} elem
     * @param {Array} options
     */
    function Plugin( elem, options )
    {
        options = $.extend( {
                'url': $( elem ).attr( 'data-source' ),
                'offset': parseInt( $( elem ).attr( 'data-offset' ) || 0 ),
                'limit': parseInt( $( elem ).attr( 'data-limit' ) || 0 )
            },
            $.fn.swissbitFilter.defaults,
            $.isPlainObject( options ) ? options : {}
        );

        this.elem = elem;

        $( elem ).on( 'keyup', 'input[type=text]', function( e ) {
            if( 13 == e.keyCode ) {
                $( this ).blur();
            }
        } );

        $( elem ).on('change', function( e ) {
            e.preventDefault();
            $( elem ).trigger( 'changed.filter' );
        } );
    };
})( jQuery );
