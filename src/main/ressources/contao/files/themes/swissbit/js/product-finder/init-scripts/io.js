"use strict";


jQuery( function( $ ) {

    $( 'input[type=checkbox], input[type=radio]' )
        .wrap( function() {
            if( $( this ).is( 'label [type]' ) ) {
                $( this ).closest( 'label' ).addClass( 'io-checkbox' ).attr( 'data-io-control', 'checkbox' );
            } else {
                return '<label data-io-control="checkbox" class="io-checkbox" for="' + $( this ).attr( 'id' ) + '" />';
            }
        } )
        .filter( ':checked' )
        .closest( '.io-checkbox' )
        .addClass( 'io-checkbox-checked' );


    $( '[data-io-control="IntervalSlider"]' ).each( function() {

        var $lowerInput = $( '[data-io-control-related="LowerHandle"]', this ),
            $upperInput = $( '[data-io-control-related="UpperHandle"]', this ),
            min = parseInt( $( this ).attr( 'data-min' ) ),
            max = parseInt( $( this ).attr( 'data-max' ) ),
            lowerValue = $lowerInput.val() || min,
            upperValue = $upperInput.val() || min,
            startValues = [lowerValue, upperValue],
            snap = false,
            step = 1,
            rangeValues = { 'min': min, 'max': max };

        if( $lowerInput.is( 'select' ) && $upperInput.is( 'select' ) ) {
            var $options = $lowerInput.find( 'option' );

            lowerValue = $lowerInput.find( 'option:selected' ).attr( 'value' ) || min;
            upperValue = $upperInput.find( 'option:selected' ).attr( 'value' ) || max;
            startValues = [lowerValue, upperValue];
            snap = true;
            step = 0;

            var distance = Math.floor( 100 / ($options.size() - 1) ),
                pos = 0;

            rangeValues = {};

            for( var i = 0, htmlElement; (htmlElement = $options.get( i )); i++ ) {

                if( $( htmlElement ).is( ':first-child' ) ) {
                    rangeValues['min'] = [parseInt( $( htmlElement ).attr( 'value' ) )];
                }
                else if( $( htmlElement ).is( ':last-child' ) ) {
                    rangeValues['max'] = [parseInt( $( htmlElement ).attr( 'value' ) )];
                } else {
                    rangeValues[(pos += distance) + '%'] = [parseInt( $( htmlElement ).attr( 'value' ) )];
                }
            }
        }

        var htmlElement = $( '<div data-io-control="IntervalSlider" />' )
            .appendTo( this )
            .get( 0 );

        $( htmlElement ).wrap( '<div class="io-control-slider" />' );

        noUiSlider.create( htmlElement, {
            'tooltips': [true, true],
            'start': startValues,
            'snap': snap,
            'step': step,
            'connect': true,
            'range': rangeValues
        } );

        $lowerInput.on( 'change', function( e ) {
            htmlElement.noUiSlider.set( [$( this ).val(), null] );
        } );
        $upperInput.on( 'change', function( e ) {
            htmlElement.noUiSlider.set( [null, $( this ).val()] );
        } );

        htmlElement.noUiSlider.on( 'change', function( values, handle ) {
            (0 == handle ? $lowerInput : $upperInput).val( parseInt( values[handle] ) );
            $( htmlElement ).trigger( 'change' );
        } );

    } );


    $( 'body' )
        .on( 'change', '.io-checkbox', function() {
            if( $( 'input[type]', this ).is( '[type="checkbox"]' ) ) {
                $( this ).toggleClass( 'io-checkbox-checked',
                    $( 'input[type=checkbox]', this ).is( ':checked' ) );
            } else if( $( 'input[type]', this ).is( '[type="radio"]' ) ) {
                $( this ).toggleClass( 'io-checkbox-checked', $( 'input[type="radio"]', this ).is( ':checked' ) );

                $( 'input[name=' + $( 'input[type="radio"]', this ).attr( 'name' ) + ']:radio:not(:checked)' )
                    .closest( '.io-checkbox-checked' )
                    .removeClass( 'io-checkbox-checked' );
            }
        } )
        .on( 'reset', 'form', function() {

            $( '.noUi-target', this ).each( function() {
                this.noUiSlider.reset();
            } );

            $( 'input[type=text]', this )
                .attr( 'value', null );

            $( 'input[type=checkbox]', this )
                .attr( 'checked', false )
                .closest( '.io-checkbox' )
                .removeClass( 'io-checkbox-checked' );
        } );
} );
