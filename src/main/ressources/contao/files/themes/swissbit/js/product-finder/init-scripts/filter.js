"use strict";

jQuery( function( $ ) {
    $( '.filter:not(.setup)' ).swissbitFilter().addClass( 'setup' );

    $( 'body' )

        .on( "click", '[data-reset="product-filter"]', function( e ) {
            e.preventDefault();
            e.stopPropagation();

            $( this ).closest( 'form' ).get( 0 ).reset();
            $( this ).closest( 'form' ).submit();
        } )
        .on( 'changed.filter', 'form.product-filter', function( e ) {
            e.preventDefault();
            $( this ).submit();
        } )
        .on( 'submit', 'form.product-filter', function( e ) {
            e.preventDefault();

            var data = $( this ).serializeArray();

            // während dem Laden alle Eingabemöglichkeiten deaktivieren!
            $( 'input, select, [data-io-control]', this ).attr( 'disabled', true );

            $.post( $( this ).attr( 'data-action' ), data, $.proxy( function( response ) {
                // Felder wieder aktivieren.
                $( 'input, select, [data-io-control]', this ).attr( 'disabled', false );

                $( '.filter', this )
                    .removeClass( 'panel-primary' )
                    .filter( function() {
                        return -1 < response.active_filters.indexOf( $( this ).attr( 'data-filter' ) );
                    } )
                    .addClass( 'panel-primary' );

                $( this ).trigger( 'change.filter' );
            }, this ) );
        } )
        .on( "click", '[data-toggle=filter]', function( e ) {
            e.preventDefault();

            $( '.filter-group-collapse', $( this ).closest( 'form.product-filter' ) )
                .animate( {
                    height: 'toggle'
                }, {
                    complete: function() {
                        $( this )
                            .toggleClass( 'in' )
                            .css( 'display', '' )
                            .closest( 'form.productfinder-filter' )
                            .toggleClass( 'filter-group-collapsed', $( this ).is( '.in' ) );
                    }
                } )
        } );
} );
