"use strict";

/**
 * Plugin Closure
 */
(function( $ ) {

    /**
     * @param {string} method
     */
    jQuery.fn.swissbitModal = function( method ) {
        var args = arguments;
        return this.each( function() {
            var plugin = $( this ).data( 'plugin' );

            if( !plugin ) {
                $( this ).data( 'plugin', plugin = new Plugin( this, args.length ? args[0] : null ) );
            }

            if( plugin[method] ) {
                return plugin[method].apply( plugin, Array.prototype.slice.call( args, 1 ) );
            } else if( method ) {
                $.error( 'Method ' + method + ' does not exist on jQuery.swissbitModal' );
            }
        } );
    };

    /**
     * @type {Object}
     */
    jQuery.fn.swissbitModal.defaults = {};

    /**
     * @param {HTMLElement} elem
     * @param {Array} options
     */
    function Plugin( elem, options )
    {
        options = $.extend( {
                'name': 'Modal'
            },
            $.fn.swissbitModal.defaults,
            $.isPlainObject( options ) ? options : {}
        );

        this.elem = elem;
        this.isLoading = false;
        this.name = options.name;

        this.iframe = $( '<iframe style="position: absolute; width: 1px; height: 1px; visibility: hidden;" width="1" height="1" name="' + this.name + '" />' )
            .appendTo( elem )
            .get( 0 );

        $( this.elem ).on( 'click', '[data-type=submit]', $.proxy( function() {
            $( this.elem ).find( 'form' ).submit();
        }, this ) );

        $( this.iframe ).on( 'load', function() {
            var $modalDialog = $( '.modal-dialog', $( this ).contents() ),
                large = $modalDialog.is( '.modal-lg' );

            if( $modalDialog.is( '.modal-dialog' ) ) {
                $( this )
                    .closest( '.modal' )
                    .attr( 'data-src', this.contentWindow.location.href )
                    .find( '.modal-dialog' )
                    .html( $modalDialog.html() )
                    .toggleClass( 'modal-lg', large )
                    .trigger( 'loaded.modal' )
                    .end()
                    .filter( ':not(.in)' )
                    .modal( 'show' );
            }
        } );
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.open = function( url ) {
        // this.isLoading = true;

        $( this.iframe ).attr( 'src', url );

        return this;
    };

    /**
     * @return {Plugin}
     */
    Plugin.prototype.alert = function( message ) {
        var url = 'product-finder/modal/alert/';

        $.post( url, { 'message': message }, function( htmlContent ) {
            $( '#Modal' )
                .attr( 'data-src', url )
                .find( '.modal-dialog' )
                .html( $( htmlContent ).find( '.modal-content' ) )
                .trigger( 'loaded.modal' )
                .end()
                .filter( ':not(.in)' )
                .modal( 'show' );
        } );

        return this;
    };

})( jQuery );
