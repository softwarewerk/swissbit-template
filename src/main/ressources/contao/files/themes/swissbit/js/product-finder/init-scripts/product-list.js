"use strict";

jQuery( function( $ ) {
    $( '.product-list' ).swissbitProductList();

    // Toggle Product Compare
    $( 'body' )
        .on( "item-toggle.product-list.swissbit", function( e, productList, itemIds ) {
            $( '.product-compare', this )
                .toggleClass( 'in', (1 < itemIds.length) )
                .find( '.number-of-products-to-compare' )
                .text( itemIds.length );
        } )
        .on( "reset.product-list.swissbit", function( e ) {
            $( '.product-compare.in', this )
                .removeClass( 'in' )
                .find( '.number-of-products-to-compare' )
                .text( 0 );
        } );
} );
