"use strict";

/**
 * Main
 */
jQuery( function( $ ) {

    $( 'header.sticky' )
        .clone()
        .removeClass( 'sticky' )
        .insertAfter( 'header' );


    $( '.table-header-affix' ).each( function() {

        $( this ).css( 'position', 'relative' );

        var $table = $( 'table', this )
                .clone()
                .find( 'tbody' ).remove().end()
                .prependTo( this )
                .addClass( 'clone table-floating-header' )
                .css( {
                    'margin-bottom': 0,
                    'position': 'absolute',
                    'z-index': 100,
                    'top': 0,
                    'left': 0
                } ),
            initialOffset = $( this ).position().top;

        $( document ).on( 'scroll-start', function() {
            $table.css( 'opacity', 0 );
        } );

        $( document ).on( 'scroll-stop', function( e, direction, scrollTop ) {
            setTimeout( function() {
                $table
                    // .css( 'top', (initialOffset + scrollTop - 2) )
                    .css( 'top', scrollTop - 100 )
                    .animate( {
                        'top': (initialOffset + scrollTop - 2),
                        'opacity': 1
                    }, 'fast' );
            } );
        } );
    } );

} );
