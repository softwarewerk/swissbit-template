<?php
/**
 * @var \Contao\FrontendTemplate $this
 */
?>
<!DOCTYPE html>
<html lang="<?= $this->language ?>"<?php if( $this->isRTL ): ?> dir="rtl"<?php endif; ?>>
<head>
    <?php $this->block( 'head' ); ?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php # = $this->viewport ?>

    <meta charset="<?= $this->charset ?>">

    <title><?= $this->title ?></title>
    <base href="<?= $this->base ?>">

    <?php $this->block( 'meta' ); ?>
    <meta name="robots" content="<?= $this->robots ?>">
    <meta name="description" content="<?= $this->description ?>">
    <meta name="generator" content="Contao Open Source CMS">
    <?php $this->endblock(); ?>

    <?= $this->framework ?>
    <?= $this->stylesheets ?>
    <?= $this->mooScripts ?>
    <?= $this->head ?>

    <?php $this->block( 'html5shiv' ); ?>
    <!--[if lt IE 9]>
    <script src="<?= TL_ASSETS_URL ?>assets/html5shiv/js/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <?php $this->endblock(); ?>

    <?php $this->block( 'fontawesome' ); ?>
    <!-- Font Awesome -->
    <link href="<?= TL_ASSETS_URL ?>files/themes/swissbit/lib/fontawesome-5.0.8/css/fontawesome-all.css"
          rel="stylesheet">
    <?php $this->endblock(); ?>

    <link rel="stylesheet" href="<?= TL_ASSETS_URL ?>files/themes/swissbit/css/main.css?<?= time(); ?>">

    <?php $this->endblock(); ?>
</head>
<body id="top" class="{{ua::class}}<?php if( $this->class ) {
    echo ' ' . $this->class;
} ?>"<?php if( $this->onload ): ?> onload="<?= $this->onload ?>"<?php endif; ?> itemscope
      itemtype="http://schema.org/WebPage">

<?php $this->block( 'body' ); ?>
<?php $this->sections( 'top' ); ?>

<?php $this->block( 'header' ); ?>
<header>

    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#NavbarCollapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="<?= '/'; ?>"><span
                            class="sr-only">Swissbit <sup>&reg;</sup></span></a>
            </div>
        </div><!-- /.container-fluid -->
    </nav>

    <?php if( $this->header ): ?>
        <?= $this->header ?>
    <?php endif; ?>

</header>
<?php $this->endblock(); ?>

<?php $this->sections( 'before' ); ?>

<?php $this->block( 'container' ); ?>
<main>
    <div class="container">
        <div class="row">
            <?php if( $this->right ): ?>
                <div class="col-xs-10">
                    <?php $this->block( 'main' ); ?>
                    <?= $this->main ?>
                    <?php $this->sections( 'main' ); ?>
                    <?php $this->endblock(); ?>
                </div>
                <div class="col-xs-2">
                    <?php $this->block( 'right' ); ?>
                    <?= $this->right ?>
                    <?php $this->endblock(); ?>
                </div>
            <?php elseif( $this->left ): ?>
                <div class="col-xs-2">
                    <?php $this->block( 'left' ); ?>
                    <?= $this->left ?>
                    <?php $this->endblock(); ?>
                </div>
                <div class="col-xs-10">
                    <?php $this->block( 'main' ); ?>
                    <?= $this->main ?>
                    <?php $this->sections( 'main' ); ?>
                    <?php $this->endblock(); ?>
                </div>
            <?php else: ?>

                <div class="col-xs-12">
                    <?php $this->block( 'main' ); ?>
                    <?= $this->main ?>
                    <?php $this->sections( 'main' ); ?>
                    <?php $this->endblock(); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</main>
<?php $this->endblock(); ?>

<?php $this->sections( 'after' ); ?>

<?php $this->block( 'footer' ); ?>
<?php if( $this->footer ): ?>
    <footer id="footer" itemscope itemtype="http://schema.org/WPFooter">
        <div class="inside">
            <?= $this->footer ?>
        </div>
    </footer>
<?php endif; ?>
<?php $this->endblock(); ?>

<?php $this->sections( 'bottom' ); ?>

<?php $this->endblock(); ?>

<!-- Libs -->
<script src="<?= TL_ASSETS_URL ?>files/themes/swissbit/lib/jquery.min.js"></script>
<script src="<?= TL_ASSETS_URL ?>files/themes/swissbit/lib/bootstrap-3.3.7/js/bootstrap.min.js"></script>

<!-- Init Scripts -->
<script src="<?= TL_ASSETS_URL ?>files/themes/swissbit/js/main.js?<?= time(); ?>"></script>
</body>
</html>
