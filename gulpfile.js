const gulp = require( 'gulp' );
const gulpSass = require( 'gulp-sass' );
const gulpAutoprefixer = require( 'gulp-autoprefixer' );
const gulpCleanCSS = require( 'gulp-clean-css' );

var nodeModules = {};

nodeModules.bootstrap = {
    basePath: './node_modules/bootstrap-sass'
};

var sassConfig = {
    errLogToConsole: true,
    includePaths: [
        nodeModules.bootstrap.basePath + '/assets/stylesheets'],
    outputStyle: 'compressed',
    precision: 10,
    sourceMap: 'sass',
    sourceComments: 'map'
};

var buildConfig = {
    srcDir: './src',
    buildTarget: './build',
};

gulp.task( 'scss', function() {
    gulp.src( buildConfig.srcDir + '/main/sass/*.scss' )
    // .pipe(sourcemaps.init())
        .pipe( gulpSass( sassConfig ) )
        .pipe( gulpAutoprefixer( {
            browsers: ['last 2 version']
        } ) )
        // .pipe( concat( 'main.min.css' ) )
        .pipe( gulpCleanCSS( { compatibility: 'ie8' } ) )
        // .pipe( sourcemaps.write( '/sourcemaps', { includeContent: false, sourceRoot: '/scss' } ) )
        .pipe( gulp.dest( buildConfig.buildTarget + '/contao/files/themes/swissbit/css/' ) );
} );

gulp.task( 'watch', ['scss'], function() {
    gulp.watch( buildConfig.srcDir + '/main/sass/**/*.scss', ['scss'] );
} );
